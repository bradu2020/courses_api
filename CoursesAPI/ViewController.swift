//
//  ViewController.swift
//  CoursesAPI
//
//  Created by XeVoNx on 14/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    struct Course: Decodable {
        let id: Int
        let name: String
        let imageUrl: String
        let number_of_lessons: Int
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //associated value + result type enum, (let c) to get a handle on all courses
        //that are coming back from the fetch call
        fetchCourseJSON { (res) in
            switch res {
            case .success(let courses):
                courses.forEach({ (course) in
                    print(course.name)
                })
            case .failure(let err):
                print("Failed to fetch course:",err)
            }
        }
        
    }
    
    //Complete on a result type (represents success/failuire) and includes associated value in each case
    fileprivate func fetchCourseJSON(completion: @escaping (Result<[Course],Error>) -> () ){
        
        let urlString = "http://api.letsbuildthatapp.com/jsondecodable/courses"
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            //check the potential err and maybe response
            if let err = err {
                completion(.failure(err))
                return
            }
            
            //success
            do {
                let courses = try JSONDecoder().decode([Course].self, from: data!)
                completion(.success(courses))
                
            } catch let jsonError{
                completion(.failure(jsonError))
            }
            
        }.resume()
    }

}

